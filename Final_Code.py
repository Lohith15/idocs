from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
import os
import openai
import pandas as pd

import numpy as np
import pandas as pd
from fpdf import FPDF
import numpy as np

openai.api_key = "a8fe99cb6b354a06913f189536cdf8fc"
openai.api_base = "https://azure-test12.openai.azure.com/"
openai.api_type='azure'
openai.api_version='2023-03-15-preview'

os.environ["OPENAI_API_TYPE"] = "azure"
os.environ["OPENAI_API_BASE"] = "https://azure-test12.openai.azure.com/"
os.environ["OPENAI_API_KEY"] = "a8fe99cb6b354a06913f189536cdf8fc"
os.environ["OPENAI_API_VERSION"] = "2023-03-15-preview"

df = pd.DataFrame()

def create_embeddings():
    raw_text = ""
    for text_files in os.listdir('www.acquisition.gov/'):
        with open('www.acquisition.gov/'+text_files,'r',encoding='utf8') as r:
            raw_text += r.read()
        
    print("Text Extracted.")
    text_splitter = CharacterTextSplitter(separator=" ",chunk_size=1000,chunk_overlap=200,length_function=len)
    texts = text_splitter.split_text(raw_text)
    embeddings = OpenAIEmbeddings(model="text-embedding-ada-002",chunk_size=1)
    # embeddings = OpenAIEmbeddings(openai_api_key=openai.api_key,chunk_size=1,model="text-embedding-ada-002")
    # docsearch=FAISS.from_texts(texts[0],embeddings)
    # docsearch.persist()
    db = FAISS.from_texts(texts, embeddings)

    print("Embeddings created.")
    db.save_local("faiss_index")

    return db

from langchain.chains.question_answering import load_qa_chain
from langchain.llms import OpenAI

def answer_question(query,docsearch):
    print("Answer Question called.")
    chain = load_qa_chain(OpenAI(deployment_id='text-completion'),chain_type="stuff")
    # chain = load_qa_chain(OpenAI(),chain_type="stuff")
    docs = docsearch.similarity_search(query)
    answer = chain.run(input_documents=docs,question=query)
    return answer

docsearch=create_embeddings()
def get_content(search_text):
    search_text = "Write 100 words on the topic "+search_text
    result = answer_question(search_text,docsearch)
    return result

df = pd.read_excel('TOC.xlsx')
df.drop([0,1,2,3,4,5], axis = 0, inplace = True)
print(df)
df.loc[len(df)+5] = ['end',0]
df.dropna(inplace = True)
df.reset_index(inplace=True)

pdf = FPDF('P', 'mm', 'A4')
pdf.add_page()
pdf.add_font('DejaVu','','DejaVuSansCondensed.ttf',uni=True)
pdf.set_font("DejaVu", size = 15)
heading = ''
sub_heading = ''
sub_sub_heading = ''
# parse_excel()
print(df.index)

for index in df.index:
    print(index)
    print(df['Unnamed: 0'][index])
    list1 = str(df['Unnamed: 0'][index]).split('.')
    if str(df['Unnamed: 0'][index]) == 'end':
        #print(index)
        break
    #print("List : ",list1)
    if len(str(df['Unnamed: 0'][index]).split('.')) == 1:
        heading = str(df['Unnamed: 0'][index]) + "   " + str(df['Unnamed: 1'][index])
        print(heading)
        if len(str(df['Unnamed: 0'][index+1]).split('.')) == 1:
            pdf.cell(200, 10, txt = heading,ln = 1, align = 'L')
            # pdf.cell(200, 10, ln = 1, txt = sample_content, align = 'L')
            sample_content = get_content(df['Unnamed: 1'][index])
            print(sample_content)
            print(df['Unnamed: 1'][index])
            pdf.multi_cell(0, 6, sample_content)
        elif len(str(df['Unnamed: 0'][index+1]).split('.')) != 1:
            pdf.cell(200, 10, txt = heading,ln = 1, align = 'L')
    elif len(str(df['Unnamed: 0'][index]).split('.')) == 2:
        sub_heading = str(df['Unnamed: 0'][index]) + "   " + str(df['Unnamed: 1'][index])
        if len(str(df['Unnamed: 0'][index+1]).split('.')) == 2 or len(str(df['Unnamed: 0'][index+1]).split('.')) == 1:
            print("2",sub_heading,index)
            pdf.cell(200, 10, txt = sub_heading, ln = 1, align = 'L')
            sample_content = get_content(df['Unnamed: 1'][index] + "In the context of "+heading)
            print(df['Unnamed: 1'][index])
            print(sample_content)
            pdf.multi_cell(0, 6, sample_content)
        elif len(str(df['Unnamed: 0'][index+1]).split('.')) != 1:
            pdf.cell(200, 10, txt = sub_heading, ln = 1, align = 'L')
            print("2con",sub_heading,index)
            continue
    elif len(str(df['Unnamed: 0'][index]).split('.')) == 3:
        print("3",index)
        sub_sub_heading = str(df['Unnamed: 0'][index]) + "   " + str(df['Unnamed: 1'][index])
        pdf.cell(200, 10, txt = sub_sub_heading,ln = 1, align = 'L')
        # pdf.cell(200, 10, ln = 1, txt = sample_content, align = 'L')
        sample_content = get_content(df['Unnamed: 1'][index] + "In the context of "+heading)
        pdf.multi_cell(0, 6, sample_content)

pdf.output("SOP-1.pdf")
