from langchain.embeddings.openai import OpenAIEmbeddings
import openai
import os

openai.api_key = "a8fe99cb6b354a06913f189536cdf8fc"
openai.api_base = "https://azure-test12.openai.azure.com/"
openai.api_type='azure'
openai.api_version='2023-03-15-preview'

os.environ["OPENAI_API_TYPE"] = "azure"
os.environ["OPENAI_API_BASE"] = "https://azure-test12.openai.azure.com/"
os.environ["OPENAI_API_KEY"] = "a8fe99cb6b354a06913f189536cdf8fc"
os.environ["OPENAI_API_VERSION"] = "2023-03-15-preview"



embeddings = OpenAIEmbeddings(model="text-embedding-ada-002")
text = "This is a test document."
query_result = embeddings.embed_query(text)
print(query_result)

